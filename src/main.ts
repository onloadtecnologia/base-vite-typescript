import { fromEvent } from 'rxjs';
import links from './mocks/links';

let section = document.querySelector<HTMLDivElement>("section");
let aside = document.querySelector<HTMLDivElement>("aside");
let btnMenu =  document.querySelector<HTMLButtonElement>("#btnMenu");
let listLinks = document.querySelector<HTMLUListElement>("#listLinks");


let changeMenu:boolean=false;
fromEvent(btnMenu!,"click").subscribe(act =>{
     changeMenu ? section!.style.gridTemplateColumns = "0% auto" : section!.style.gridTemplateColumns = "15% auto"
     changeMenu = !changeMenu;    
});

let ulLinks:string="";
links.forEach(link => {
     let mylink=`<li><a href="${link.url}">${link.label}</a></li>`
     ulLinks +=mylink;
});
listLinks!.innerHTML=ulLinks



export default {}
