let links =
    [
        {
            url: "#",
            label: "Home",
            permission: ["ADMIN"]

        },
        {
            url: "#",
            label: "Cadastro",
            permission: ["ADMIN"]

        }, {
            url: "#",
            label: "Contatos",
            permission: ["ADMIN"]

        }, {
            url: "#",
            label: "Produtos",
            permission: ["ADMIN"]

        }, {
            url: "#",
            label: "Sobre",
            permission: ["ADMIN"]
        }
    ]

export default links;